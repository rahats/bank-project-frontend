import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { SaveFullInfoComponent } from './save-full-info/save-full-info.component';
import { CreditCalculateComponent } from './credit-calculate/credit-calculate.component';
import { CreditRequestComponent } from './credit-request/credit-request.component';
import { CreditRequisitionsComponent } from './credit-requisitions/credit-requisitions.component';
import { DepositComponent } from './deposit/deposit.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RegisterComponent,
    LoginComponent,
    SaveFullInfoComponent,
    CreditCalculateComponent,
    CreditRequestComponent,
    CreditRequisitionsComponent,
    DepositComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
