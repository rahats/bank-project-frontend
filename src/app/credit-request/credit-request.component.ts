import { Component, OnInit } from '@angular/core';
import { catchError, finalize } from 'rxjs';
import { CreditService } from '../credit.service';

@Component({
  selector: 'app-credit-request',
  templateUrl: './credit-request.component.html',
  styleUrls: ['./credit-request.component.css']
})
export class CreditRequestComponent implements OnInit {

  isProcessing: boolean = false;
  hasMessage: boolean = false;
  message: boolean = false;

  sum: number = 0;
  limitationTime: number = 0;
  rate: number = 0;

  constructor(
    private creditService: CreditService
  ) { }

  ngOnInit(): void {
  }

  onSend() {
    this.isProcessing = true;
    this.creditService.request(this.sum, this.limitationTime, this.rate)
      .pipe(
        catchError(err => {
          return '';
        }),
        finalize(() => {
          this.isProcessing = false;
        })
      )
      .subscribe(res => {
        this.hasMessage = true;
        this.message = res.message;
        console.log(res);
      });
  }

}
