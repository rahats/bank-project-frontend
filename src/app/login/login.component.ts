import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, finalize } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  phone: string = '';
  password: string = '';
  isProcessingLogin: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onLogin() {
    this.isProcessingLogin = true;
    this.authService.login(this.phone, this.password)
      .pipe(
        catchError(err => {
          return '';
        }),
        finalize(() => {
          this.isProcessingLogin = false;
        })
      )
      .subscribe(res => {
        this.authService.setToken(res.accessToken);
        this.router.navigate(['/save-full-info']);
      });
  }

}
