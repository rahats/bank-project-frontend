export interface SaveFullInfo {
    iin: string,
    lastname: string,
    firstname: string,
    middleName: string,
    phoneNumber: string,
    birthDate: string,
    gender: string,
    documentNumber: string,
    documentIssuedBy: string,
    documentIssuedDate: string,
    documentValidUntilDate: string,
    salary: number,
    communalPayment: number
}
