import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreditCalculateComponent } from './credit-calculate/credit-calculate.component';
import { CreditRequestComponent } from './credit-request/credit-request.component';
import { CreditRequisitionsComponent } from './credit-requisitions/credit-requisitions.component';
import { DepositComponent } from './deposit/deposit.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SaveFullInfoComponent } from './save-full-info/save-full-info.component';

const routes: Routes = [
  { path: 'register', component: RegisterComponent },
  { path: 'login', component: LoginComponent },
  { path: 'save-full-info', component: SaveFullInfoComponent },
  { path: 'credit/calculate', component: CreditCalculateComponent },
  { path: 'credit/request', component: CreditRequestComponent },
  { path: 'credit/requisitions', component: CreditRequisitionsComponent },
  { path: 'deposit', component: DepositComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
