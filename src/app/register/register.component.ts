import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { catchError, finalize, map, throwError } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  phone: string = '';
  password: string = '';
  isProcessingRegister: boolean = false;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onRegister(): void {    
    this.isProcessingRegister = true;
    this.authService.register(this.phone, this.password)
      .pipe(
        catchError((err) => {
          return '';
        }),
        finalize(() => {
          this.isProcessingRegister = false;
        })
      )
      .subscribe(res => {
        this.router.navigate(['/login']);
      });
  }

}
