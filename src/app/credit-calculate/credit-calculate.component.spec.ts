import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditCalculateComponent } from './credit-calculate.component';

describe('CreditCalculateComponent', () => {
  let component: CreditCalculateComponent;
  let fixture: ComponentFixture<CreditCalculateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditCalculateComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditCalculateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
