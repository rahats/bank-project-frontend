import { Component, OnInit } from '@angular/core';
import { catchError, finalize } from 'rxjs';
import { CreditService } from '../credit.service';

@Component({
  selector: 'app-credit-calculate',
  templateUrl: './credit-calculate.component.html',
  styleUrls: ['./credit-calculate.component.css']
})
export class CreditCalculateComponent implements OnInit {
  isProcessing: boolean = false;
  message: any = '';

  sum: number = 0;
  limitationTime: number = 0;
  rate: number = 0;

  constructor(
    private creditService: CreditService
  ) { }

  ngOnInit(): void {
  }

  onSend() {
    this.isProcessing = true;
    this.creditService.calculate(this.sum, this.limitationTime, this.rate)
      .pipe(
        catchError(err => {
          return '';
        }),
        finalize(() => {
          this.isProcessing = false;
        })
      )
      .subscribe(res => {
        this.message = res;
        console.log(res);
      });
  }

}
