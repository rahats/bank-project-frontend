import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditRequisitionsComponent } from './credit-requisitions.component';

describe('CreditRequisitionsComponent', () => {
  let component: CreditRequisitionsComponent;
  let fixture: ComponentFixture<CreditRequisitionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreditRequisitionsComponent ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditRequisitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
