import { Component, OnInit } from '@angular/core';
import { catchError, finalize } from 'rxjs';
import { CreditService } from '../credit.service';

@Component({
  selector: 'app-credit-requisitions',
  templateUrl: './credit-requisitions.component.html',
  styleUrls: ['./credit-requisitions.component.css']
})
export class CreditRequisitionsComponent implements OnInit {

  items: any;

  constructor(
    private creditService: CreditService
  ) { }

  ngOnInit(): void {
    this.creditService.getRequisitions()
      .pipe(
        catchError(err => {
          return '';
        })
      )
      .subscribe(res => {
        this.items = res;
        console.log(res);

      });
  }

  onCancel(id: number) {
    this.creditService.cancel(id)
      .pipe(
        catchError(err => {
          return '';
        })
      )
      .subscribe(res => {
        console.log(res);
        this.items.splice(id, 1);
      });
    console.log(id);
  }

}
