import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { SaveFullInfo } from './requests/save-full-info';

@Injectable({
  providedIn: 'root'
})
export class SystemService {
  base: string = 'http://127.0.0.1:8081/api/v1/system';

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  saveFullInfo(request: SaveFullInfo): Observable<any> {
    return this.http.post<any>(this.base + '/save/full/info', request, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }
}
