import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class DepositService {
  base: string = 'http://127.0.0.1:8081/api/v1/deposit';

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  open(maxReplenishment: number, limitationTime: number): Observable<any> {
    return this.http.post(this.base + '/open', {
      maxReplenishment: Number(maxReplenishment),
      limitationTime: Number(limitationTime)
    }, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }

  replenishment(amount: number): Observable<any> {
    return this.http.post(this.base + '/replenishment', {
      replenishment: Number(amount)
    }, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }

  withdrawal(amount: number): Observable<any> {
    return this.http.get(this.base + '/withdrawal/' + amount, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }

  close(): Observable<any> {
    return this.http.get(this.base + '/close', {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    })
  }


}
