import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CreditService {
  base: string = 'http://127.0.0.1:8081/api/v1/credit';

  constructor(
    private http: HttpClient,
    private authService: AuthService
  ) { }

  calculate(sum: number, limitationTime: number, rate: number): Observable<any> {
    return this.http.post<any>(this.base + '/calculate', {
      sum: Number(sum),
      limitationTime: Number(limitationTime),
      rate: Number(rate)
    }, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }

  request(sum: number, limitationTime: number, rate: number): Observable<any> {
    return this.http.post<any>(this.base + '/request', {
      sum: Number(sum),
      limitationTime: Number(limitationTime),
      rate: Number(rate)
    }, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }

  getRequisitions(): Observable<any> {
    return this.http.get<any>(this.base + '/all/requisitions', {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }

  cancel(id: number): Observable<any> {
    console.log(id)
    return this.http.post<any>(this.base + '/cancel', {
      id: Number(id)
    }, {
      headers: {
        'Authorization': 'Bearer ' + this.authService.token
      }
    });
  }

}
