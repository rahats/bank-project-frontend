import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  @Output() onTokenSet = new EventEmitter();
  base: string = 'http://127.0.0.1:8081/api/v1/public';
  token: string = '';

  constructor(
    private http: HttpClient,
  ) { }

  getUser(): User {
    var user: User = {
      id: 1,
      phone: '1235'
    };
    return user;
  }

  register(phoneNumber: string, password: string): Observable<any> {
    // return this.http.get<any>('https://yesno.wtf/api');
    return this.http.post<any>(this.base + '/register', {
      phoneNumber: phoneNumber,
      password: password
    });
  }

  login(phoneNumber: string, password: string): Observable<any> {
    return this.http.post<any>(this.base + '/sign-in', {
      phoneNumber: phoneNumber,
      password: password
    });
  }

  setToken(token: string) {
    this.token = token;
    console.log(token);

    if (token) {
      this.onTokenSet.emit('change');
    }
  }

}
