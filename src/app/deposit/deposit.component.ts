import { Component, OnInit } from '@angular/core';
import { catchError, finalize } from 'rxjs';
import { DepositService } from '../deposit.service';

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.component.html',
  styleUrls: ['./deposit.component.css']
})
export class DepositComponent implements OnInit {

  hasMessage: boolean = false;
  message: string = '';
  isProcessing: boolean = false;

  maxReplenishment: number = 0;
  limitationTime: number = 0;
  replenishment: number = 0;
  withdrawal: number = 0;

  constructor(
    private depositService: DepositService
  ) { }

  ngOnInit(): void {
  }

  onOpen() {
    this.isProcessing = true;
    this.depositService.open(this.maxReplenishment, this.limitationTime)
      .pipe(
        catchError(err => {
          return '';
        }),
        finalize(() => {
          this.isProcessing = false;
        })
      )
      .subscribe(res => {
        console.log(res);
        this.message = res.message;
      });
  }

  onReplenish() {
    this.isProcessing = true;
    this.depositService.replenishment(this.replenishment)
      .pipe(
        catchError(err => {
          console.log(err.message);

          return '';
        }),
        finalize(() => {
          this.isProcessing = false;
        })
      )
      .subscribe(res => {
        console.log(res);
        this.message = res.message;
      });
  }

  onWithdraw() {
    this.isProcessing = true;
    this.depositService.withdrawal(this.withdrawal)
      .pipe(
        catchError(err => {
          console.log(err.message);

          return '';
        }),
        finalize(() => {
          this.isProcessing = false;
        })
      )
      .subscribe(res => {
        console.log(res);
        this.message = res.message;
      });
  }

  onClose() {
    this.isProcessing = true;
    this.depositService.close()
      .pipe(
        catchError(err => {
          console.log(err.message);

          return '';
        }),
        finalize(() => {
          this.isProcessing = false;
        })
      )
      .subscribe(res => {
        console.log(res);
        this.message = res.message;
      });
  }

}
