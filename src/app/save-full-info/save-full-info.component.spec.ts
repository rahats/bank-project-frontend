import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveFullInfoComponent } from './save-full-info.component';

describe('SaveFullInfoComponent', () => {
  let component: SaveFullInfoComponent;
  let fixture: ComponentFixture<SaveFullInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaveFullInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveFullInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
