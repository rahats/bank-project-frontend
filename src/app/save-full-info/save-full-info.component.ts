import { Component, OnInit } from '@angular/core';
import { catchError, finalize } from 'rxjs';
import { SaveFullInfo } from '../requests/save-full-info';
import { SystemService } from '../system.service';

@Component({
  selector: 'app-save-full-info',
  templateUrl: './save-full-info.component.html',
  styleUrls: ['./save-full-info.component.css']
})
export class SaveFullInfoComponent implements OnInit {
  isProcessing: boolean = false;

  iin: string = '';
  lastname: string = '';
  firstname: string = '';
  middleName: string = '';
  phoneNumber: string = '';
  birthDate: string = '';
  gender: string = '';
  documentNumber: string = '';
  documentIssuedBy: string = '';
  documentIssuedDate: string = '';
  documentValidUntilDate: string = '';
  salary: number = 0;
  communalPayment: number = 0;

  constructor(
    private systemService: SystemService
  ) { }

  ngOnInit(): void {
  }

  onSend() {
    this.isProcessing = true;

    this.systemService.saveFullInfo({
      iin: this.iin,
      lastname: this.lastname,
      firstname: this.firstname,
      middleName: this.middleName,
      phoneNumber: this.phoneNumber,
      birthDate: this.birthDate,
      gender: this.gender,
      documentNumber: this.documentNumber,
      documentIssuedBy: this.documentIssuedBy,
      documentIssuedDate: this.documentIssuedDate,
      documentValidUntilDate: this.documentValidUntilDate,
      salary: this.salary,
      communalPayment: this.communalPayment
    })
      .pipe(
        catchError(err => {
          return '';
        }),
        finalize(() => {
          this.isProcessing = false;
        })
      )
      .subscribe(res => {
        console.log(res);
      });
  }

}
